# PDF Extractor

## Overview

This repo allows to extract specific pages from a PDF file saving into another.

All the user needs is: install the python dependencies, configuring the yaml file, and run the program.

The final result of the project with the default settings, must be in this way:

```yaml
pdf_extractor/
   |
   |_ tests/
   |_.gitignore
   |_.gitlab-ci.yml
   |__init__.py
   |_config.yml
   |_extract.py
   |_README.md
   |_requirements.txt
   |_<pdf to be processed>.pdf
   |_<pdf with specific pages>.pdf

```

## Requirements

This program use Python 3.8.5. Maybe another Python 3.x could work

Both, PyPDF2 and PyYAML, must be installed. For this, we must type:

```bash
$ pip install -r requirements.txt
```

## How to Use

### Setting the YAML File

The `config.yaml` file, has all the information we need. For example:

```yaml
pdf_name: test

pdf_path: ./test.pdf

output_folder: "."

pages_selected: 
  - 1
  - 2
  - { start: 8, end: 10 }
```

Here, the *pdf_name* key asks only for the filename.

The *pdf_path* key asks for the path of the pdf file to be extracted.

*pages_selected* it's used to specify the pages for being extracted. If the item is a dictionary, for example `{ start: 8, end: 10}`, the program will add that inclusive and closed range to the final pdf file.

In this example, the program will create a new pdf file with the content of the pages, 1, 2, 8, 9, and 10.

For the nature of the program, we could extract multiple chunk of the pdf file. For this, we should specify in the yaml file, for example:

```yaml
pages_selected: 
  - { start: 1, end: 3 }
  - { start: 8, end: 10 }
```

Here, we get the content of pages, from 1 to 3, and from 8, to 10. In other words, pages 1, 2, 3, 8, 9, and 10.

Finally, the result pdf file will be saved in the directory specified by the key *output_folder*, in a file with a prefix *splited_*. 

By default, the output folder inside the folder project.

### Running the Program

If all the configuration in the yaml file is ok, then we can type in the terminal inside the project:

```bash
$ python3 extract.py
# or
$ python extract.py
```