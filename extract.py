
from PyPDF2 import PdfFileWriter
from PyPDF2 import PdfFileReader

from yaml import safe_load

class Config:

    def __init__(self, kw):
        self.__dict__.update(**kw)

class Extract:

    def __init__(self, config_yaml):
        
        self.config = self.get_config_object(config_yaml)
        
        self.pages_selected = []
        self.output_path = f'{self.config.output_folder}/splited_{self.config.pdf_name}.pdf'
        self.pdf_path = self.config.pdf_path

    def write_stream_to_pdf(self, pdf_writter, output_path):

        with open(output_path, "wb") as output_stream:
            pdf_writter.write(output_stream)

    def delete_previous_splited_file(self, file_to_delete):
        
        import os

        try:
            if os.path.exists(file_to_delete):
                os.remove(file_to_delete)
        except PermissionError:
            exit(f"Error: Close the file \"{file_to_delete}\" first")

    def get_config_object(self, config_yaml):

        with open(config_yaml, 'r') as config_file:
            config_dict = safe_load(config_file)

        return Config(config_dict)

    def page_writer(self, pdf_file, pages_selected):
            
        pdf_writer = PdfFileWriter()

        for page in pages_selected:
            pdf_writer.addPage(page)

        return pdf_writer

    def get_pages(self, pdf_path) -> list:
        
        input_pdf = PdfFileReader(pdf_path)
        pages = []

        for item in self.config.pages_selected:
            if type(item) is dict:
                start = item.get('start') - 1
                end = item.get('end')
                for page_number in range(start, end):
                    pages.append(input_pdf.getPage(page_number))
            elif type(item) is int:
                pages.append(input_pdf.getPage(item - 1))

        return pages

    def extract(self):
        
        self.delete_previous_splited_file(self.output_path)
        
        with open(self.pdf_path, "rb") as pdf_file:    
            
            self.pages_selected = self.get_pages(self.pdf_path)

            pdf_writer = self.page_writer(pdf_file, self.pages_selected)

            self.write_stream_to_pdf(pdf_writer, self.output_path)

if __name__ == '__main__':
    extract = Extract('./config.yml')
    extract.extract()