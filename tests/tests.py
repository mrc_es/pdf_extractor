
import pytest

from pdf_extractor.extract import Extract

def test_dummy_pass():

    assert 1 == 1

def test_dummy_fail():

    assert 1 == 1

def test_get_config_object():
    
    extract = Extract('./tests/test_config.yml')

    assert extract.config.pages_selected[0] == 1
    assert extract.config.pages_selected[1] == 2
    assert extract.config.pages_selected[3] == { "start": 6, "end": 9 }
    
def test_get_pages():

    extract = Extract('./tests/test_config.yml')

    pages_list = extract.get_pages(extract.pdf_path)
    
    assert len(pages_list) == 10

def test_extract():

    from PyPDF2 import PdfFileReader

    extract = Extract('./tests/test_config.yml')
    extract.extract()

    with open(extract.output_path, "rb") as pdf_file:    
           
        input_pdf = PdfFileReader(pdf_file)

        num_pages = input_pdf.getNumPages()

        assert num_pages == 10
